**Cryptocurrency IOS Application (Digipay code challenge)**
**written in swift 5 by Amirreza Ashouri**

**Programing Paradigms:**
    OOP,
    POP,
    Functional

**Architecture:**
1. MVVM+Bindable for business logic.
2. Coordinator/Router design for navigation logic.
3. CoreData for data Repository.

**Persistence:** 
- Core Data and NSFetchResultController to binding data.    
- DatabaseManager class is a singleton to manage database operations.

**Network:**
- URLSession Service layer.
- Endpoint Specifications to adding a new service just by defining a new Endpoint.
- Factory pattern for making requests.
- using coinmarketcap.com api to fetch remote data.


**UI:**
- using UIKit
- Fully programatic ui using Utils module(my own module)
- Animation using UIView.animate for handleing Sort/Filter View presentation.
- Dark and light mode.

**Tests:**
- Includes cryptocurrenciesTests target using XCTestCase for about %80 coverage.

//
//  DetailHeaderView.swift
//  cryptocurrencies
//
//  Created by amirreza on 7/17/21.
//

import UIKit
import Utils

final class DetailHeaderView: UIView {
        
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    private func setupView() {
        backgroundColor = UIColor.getColor(for: .background)
        let height = 6 * padding(.xLarge)
        let logoImageView = UIImageView(cornerRadius: height / 2, contentMode: .scaleAspectFit, image: #imageLiteral(resourceName: "logo"))
        addSubview(logoImageView)
        logoImageView.centerInSuperview(size: .init(width: height, height: height))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

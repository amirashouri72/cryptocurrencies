//
//  DetailInformationView.swift
//  cryptocurrencies
//
//  Created by amirreza on 7/17/21.
//

import UIKit
import Utils

final class DetailInformationView: UIView {
    
    private let viewModel: DetailViewModel
    
    init(_ viewModel: DetailViewModel) {
        self.viewModel = viewModel
        super.init(frame: .zero)
        setupView()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        roundCorners(corners: [.topLeft, .topRight], radius: padding(.medium), borderWidth: 1, borderColor: UIColor.getColor(for: .secondary) ?? .black)
    }
    
    private func setupView() {
        backgroundColor = UIColor.getColor(for: .primary)
        var hStacks = [UIStackView]()
        
        viewModel.detailItems.forEach { item in
            let iconImageView = UIImageView(cornerRadius: 0, contentMode: .scaleAspectFit, image: .init(named: item.icon.rawValue))
            iconImageView.constrainWidth(constant: padding(.large))
            let keyLabel = UILabel(text: item.key, font: .systemFont(ofSize: 14), numberOfLines: 1, textAlign: .left, ishidden: false, Color: UIColor.getColor(for: .background) ?? .blue)
            keyLabel.constrainWidth(constant: 5 * padding(.xLarge))
            let valueLabel = UILabel(text: item.value, font: .systemFont(ofSize: 14), numberOfLines: 0, textAlign: .left, ishidden: false, Color: UIColor.getColor(for: .background) ?? .blue)
            hStacks.append(.init([iconImageView, keyLabel, valueLabel], axis: .horizontal, distribution: .fill, alignment: .center, spacing: padding(.small)))
        }
        
        let vStackContainer = UIStackView(hStacks, axis: .vertical, distribution: .fillEqually, alignment: .fill, spacing: padding(.small))
        addSubview(vStackContainer)
        vStackContainer.fillSuperview(padding: .init(top: padding(.medium), left: padding(.medium), bottom: padding(.medium), right: padding(.medium)))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//
//  DetailCoordinator.swift
//  cryptocurrencies
//
//  Created by amirreza on 7/17/21.
//

import UIKit

class DetailCoordinator: BaseCoordinator {
    
    private let router: RouterProtocol
    private let cryptocurrency: Cryptocurrency
    init(router: RouterProtocol, cryptocurrency: Cryptocurrency) {
        self.router = router
        self.cryptocurrency = cryptocurrency
    }
    
    override func start() {
        
        // prepare the associated view and injecting its viewModel
        let viewModel = DetailViewModel(cryptocurrency: cryptocurrency)
        let homeVC = DetailViewController(with: viewModel)
        
        // select back button
        viewModel.didSelectBackBindable.bind { [weak self] _ in
            guard let self = self else { return }
            self.router.pop(true)
        }
        
        router.push(homeVC, isAnimated: true, onNavigateBack: self.isCompleted)
    }
    
    /// This will show detail view
    func showDetail(of cryptocurrency: Cryptocurrency, in router: RouterProtocol) {
        //let newCoordinator = FirstCoordinator(router: router)
        // self.start(coordinator: newCoordinator)
    }
}

//
//  DetailViewController.swift
//  cryptocurrencies
//
//  Created by amirreza on 7/17/21.
//

import UIKit
import Utils

final class DetailViewController: UIViewController {
    
    private let viewModel: DetailViewModel
    private let detailHeaderView = DetailHeaderView()
    private let detailInformationView: DetailInformationView
    
    init(with viewModel: DetailViewModel) {
        self.viewModel = viewModel
        self.detailInformationView = DetailInformationView(viewModel)
        super.init(nibName: nil, bundle: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    private func setupView() {
        view.backgroundColor = UIColor.getColor(for: .background)
        detailHeaderView.constrainHeight(constant: 10 * padding(.xLarge))
        let vStackContainer = UIStackView([detailHeaderView, detailInformationView], axis: .vertical, distribution: .fill, alignment: .fill, spacing: 0)
        view.addSubview(vStackContainer)
        vStackContainer.fillSuperview()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//
//  DetailModel.swift
//  cryptocurrencies
//
//  Created by amirreza on 7/17/21.
//

import Foundation

struct DetailItemModel {
    let icon: DetailIcon
    let key: String
    let value: String
}

enum DetailIcon: String {
    case dollar = "usDollarCircled"
    case symbol = "symbol"
    case type = "type"
    case tag = "tag"
    case marketCap = "market-cap"
    case percent = "percent"
}

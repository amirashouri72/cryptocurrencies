//
//  DetailViewModel.swift
//  cryptocurrencies
//
//  Created by amirreza on 7/17/21.
//

import Foundation
import Utils

final class DetailViewModel {
    
    //MARK:- Bindable
    
    let didSelectBackBindable = Bindable<Bool>()
    let cryptocurrency: Cryptocurrency
    let detailItems: [DetailItemModel]
    
    init(cryptocurrency: Cryptocurrency) {
        self.cryptocurrency = cryptocurrency
        self.detailItems = DetailViewModel.generateDetailItems(with: cryptocurrency)
    }
    
    static func generateDetailItems(with cryptocurrency: Cryptocurrency) -> [DetailItemModel] {
        
        let priceItem = DetailItemModel.init(icon: .dollar, key: "Price:", value: cryptocurrency.price.priceRepresentation())
        let symbolItem = DetailItemModel.init(icon: .symbol, key: "Symbol:", value: cryptocurrency.symbol ?? "-")
        let typeItem = DetailItemModel.init(icon: .type, key: "Crypto Type:", value: cryptocurrency.cryptocurrency_type ?? "")
        let tagsItem = DetailItemModel.init(icon: .tag, key: "tags:", value: cryptocurrency.tags ?? "")
        let percentItem = DetailItemModel.init(icon: .percent, key: "Percent change in 24h:", value: "%" + cryptocurrency.percent_change_24h.description)
        let marketCapItem = DetailItemModel.init(icon: .marketCap, key: "Market Cap:", value: cryptocurrency.market_cap.description)
        return [priceItem, symbolItem, typeItem, tagsItem, percentItem, marketCapItem]
    }
}

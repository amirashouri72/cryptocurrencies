//
//  CryptocurrencyTableViewCell.swift
//  cryptocurrencies
//
//  Created by amirreza on 7/13/21.
//

import UIKit
import Utils

final class CryptocurrencyTableViewCell: UITableViewCell {
    static let identifier = "cryptoCell"
    static let cellHeight: CGFloat = 80
    
    var dataSource: Cryptocurrency? {
        didSet {
            nameLabel.text = dataSource?.name
            priceLabel.text = dataSource?.price.priceRepresentation()
            percentLabel.text = "%" + (dataSource?.percent_change_24h.description ?? "0")
            dateLabel.text = dataSource?.last_updated?.toString()
        }
    }
    
    private let nameLabel = UILabel(text: "", font: UIFont.systemFont(ofSize: 16), numberOfLines: 1, textAlign: .left, ishidden: false, Color: UIColor.getColor(for: .primary) ?? .black)
    private let priceLabel = UILabel(text: "", font: UIFont.systemFont(ofSize: 14), numberOfLines: 1, textAlign: .left, ishidden: false, Color: UIColor.getColor(for: .primary) ?? .black)
    private let dateLabel = UILabel(text: "", font: UIFont.systemFont(ofSize: 16), numberOfLines: 1, textAlign: .right, ishidden: false, Color: UIColor.getColor(for: .primary) ?? .black)
    private let percentLabel = UILabel(text: "", font: UIFont.systemFont(ofSize: 14), numberOfLines: 1, textAlign: .right, ishidden: false, Color: UIColor.getColor(for: .primary) ?? .black)
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        setupView()
    }
    
    private func setupView() {
        let lineView = UIView()
        lineView.backgroundColor = UIColor.getColor(for: .secondary)
        lineView.constrainHeight(constant: 0.5)
        let leftVStack = UIStackView([nameLabel, priceLabel], axis: .vertical, distribution: .fill, alignment: .fill, spacing: padding(.small))
        let rightVStack = UIStackView([dateLabel, percentLabel], axis: .vertical, distribution: .fill, alignment: .fill, spacing: padding(.small))
        let hStackContainer = UIStackView([leftVStack, rightVStack], axis: .horizontal, distribution: .fill, alignment: .center, spacing: padding(.small))
        let vStackContainer = UIStackView([hStackContainer, lineView], axis: .vertical, distribution: .fill, alignment: .fill, spacing: padding(.small))
        addSubview(vStackContainer)
        vStackContainer.fillSuperview(padding: .init(top: 0, left: padding(.small), bottom: 0, right: padding(.small)))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//
//  CryptocurrencyView.swift
//  cryptocurrencies
//
//  Created by amirreza on 7/13/21.
//

import UIKit
import Utils
import CoreData

final class CryptocurrencyView: UIView, NSFetchedResultsControllerDelegate {
    
    let tableView: UITableView = {
        let tv = UITableView()
        tv.separatorStyle = .none
        tv.backgroundColor = .clear
        tv.register(CryptocurrencyTableViewCell.self, forCellReuseIdentifier: CryptocurrencyTableViewCell.identifier)
        return tv
    }()
    
    let viewModel: CryptocurrencyViewModel
    
    lazy var fetchedResultsController: NSFetchedResultsController<Cryptocurrency> = {

        let context = DatabaseManager.shared.persistentContainer.viewContext
        
        let request: NSFetchRequest<Cryptocurrency> = Cryptocurrency.fetchRequest()
        request.sortDescriptors = [
            NSSortDescriptor(key: CryptocurrencySortKey.price.rawValue, ascending: false)
        ]
        
        let frc = NSFetchedResultsController(fetchRequest: request, managedObjectContext: context, sectionNameKeyPath: viewModel.options.sort.rawValue, cacheName: nil)
        frc.delegate = self
        do {
            try frc.performFetch()
        } catch let err {
            print(err)
        }

        return frc
    }()
    
    init(_ viewModel: CryptocurrencyViewModel) {
        self.viewModel = viewModel
        super.init(frame: .zero)
        tableView.delegate = self
        tableView.dataSource = self
        
        setupView()
        setupObservers()
    }
    
    private func setupView() {
        addSubview(tableView)
        tableView.anchor(top: safeAreaLayoutGuide.topAnchor, leading: leadingAnchor, bottom: safeAreaLayoutGuide.bottomAnchor, trailing: trailingAnchor, padding: .init(top: padding(.small), left: 0, bottom: CryptocurrencyView.configurations.sortAndFiltersViewTopPadding, right: 0))
    }
    
    private func setupObservers() {
        viewModel.databaseObserverBindable.bind {[weak self] _ in
            DispatchQueue.main.async {
                self?.performFetch()
            }
        }
    }
    
    private func performFetch() {
        var predicates = [NSPredicate]()
        if viewModel.options.tag != .all {
            predicates.append(NSPredicate(format: "tags CONTAINS %@", viewModel.options.tag.rawValue))
        }
        if viewModel.options.cryptocurrency_type != .all {
            predicates.append(NSPredicate(format: "cryptocurrency_type == %@", viewModel.options.cryptocurrency_type.rawValue))
        }
        
        let predicate = NSCompoundPredicate(andPredicateWithSubpredicates: predicates)
        fetchedResultsController.fetchRequest.predicate = predicate

        fetchedResultsController.fetchRequest.sortDescriptors = [
            NSSortDescriptor(key: viewModel.options.sort.rawValue, ascending: viewModel.options.sort_dir == .asc)
        ]
        
        do {
            try fetchedResultsController.performFetch()
            self.tableView.reloadData()
        }catch {
            print(error)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    struct configurations {
        static let sortAndFiltersViewHeight: CGFloat = 0.8 * UIScreen.main.bounds.height
        static let paginationOffset: Int = 20
        static let sortAndFiltersViewTopPadding: CGFloat = 2 * padding(.xLarge)
    }
}

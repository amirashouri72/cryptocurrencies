//
//  SortAndFilterCollectionCell.swift
//  cryptocurrencies
//
//  Created by amirreza on 7/16/21.
//

import UIKit
import Utils

final class SortAndFilterCollectionCell: UICollectionViewCell {
    
    static let identifier = "SortCell"
    
    var dataSorce: (title: String, isSelected: Bool)? {
        didSet {
            titleLabel.text = dataSorce?.title
            titleLabel.textColor = dataSorce?.isSelected == true ? UIColor.getColor(for: .secondary) : UIColor.getColor(for: .background)
        }
    }
    
    private let titleLabel = UILabel(text: "", font: UIFont.systemFont(ofSize: 16), numberOfLines: 1, textAlign: .center, ishidden: false, Color: UIColor.getColor(for: .background) ?? .black)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    private func setupView() {
        addSubview(titleLabel)
        titleLabel.fillSuperview()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

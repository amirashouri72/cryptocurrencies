//
//  SortAndFilters+CollectionView.swift
//  cryptocurrencies
//
//  Created by amirreza on 7/15/21.
//

import UIKit
import Utils

extension SortAndFiltersView: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return viewModel.cryptocurrency.sortAndFilterDataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let view = collectionView.dequeueReusableSupplementaryView(ofKind: SortAndFiltersHeaderView.kind, withReuseIdentifier: SortAndFiltersHeaderView.identifier, for: indexPath) as? SortAndFiltersHeaderView
        let title = viewModel.cryptocurrency.sortAndFilterDataSource[indexPath.section].title
        view?.datasource = title.rawValue
        return view ?? UICollectionReusableView()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.cryptocurrency.sortAndFilterDataSource[section].values.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SortAndFilterCollectionCell.identifier, for: indexPath) as? SortAndFilterCollectionCell
        let item = viewModel.cryptocurrency.sortAndFilterDataSource[indexPath.section].values[indexPath.row]
        let section = viewModel.cryptocurrency.sortAndFilterDataSource[indexPath.section].title
        let isSelected = viewModel.checkIfIsSelected(sectionTitle: section, itemTitle: item)
        cell?.dataSorce = (item, isSelected)
        return cell ?? UICollectionViewCell()
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let item = viewModel.cryptocurrency.sortAndFilterDataSource[indexPath.section].values[indexPath.row]
        let section = viewModel.cryptocurrency.sortAndFilterDataSource[indexPath.section].title
        viewModel.didSelectFilter(with: section, itemTitle: item)
        collectionView.reloadSections([indexPath.section])
    }
}

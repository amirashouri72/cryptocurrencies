//
//  SortAndFiltersHeaderView.swift
//  cryptocurrencies
//
//  Created by amirreza on 7/16/21.
//

import UIKit
import Utils

final class SortAndFiltersHeaderView: UICollectionReusableView {
    
    static let identifier = "HeaderCell"
    static let kind = "header"
    
    var datasource: String? {
        didSet {
            label.text = datasource
        }
    }
    
    private let label = UILabel(text: "", font: UIFont.systemFont(ofSize: 14), numberOfLines: 1, textAlign: .left, ishidden: false, Color: UIColor.getColor(for: .background) ?? .black)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    private func setupView() {
        let line = UIView()
        line.backgroundColor = UIColor.getColor(for: .secondary)
        line.constrainHeight(constant: 1)
        let vStackContainer = UIStackView([label, line], axis: .vertical, distribution: .fill, alignment: .fill, spacing: padding(.small))
        addSubview(vStackContainer)
        vStackContainer.fillSuperview()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//
//  SortAndFiltersView.swift
//  cryptocurrencies
//
//  Created by amirreza on 7/15/21.
//

import UIKit
import Utils

final class SortAndFiltersView: UIView {
    
    private let collectionView: UICollectionView = {
        let layout = SortAndFiltersView.createLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.register(SortAndFilterCollectionCell.self, forCellWithReuseIdentifier: SortAndFilterCollectionCell.identifier)
        cv.register(SortAndFiltersHeaderView.self, forSupplementaryViewOfKind: SortAndFiltersHeaderView.kind, withReuseIdentifier: SortAndFiltersHeaderView.identifier)
        cv.backgroundColor = .clear
        cv.isScrollEnabled = false
        return cv
    }()
    
    static func createLayout() -> UICollectionViewLayout {
        let layout = UICollectionViewCompositionalLayout { (sectionIndex: Int,
                                                            layoutEnvironment: NSCollectionLayoutEnvironment) -> NSCollectionLayoutSection? in
            
            let footerHeaderSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0),
                                                          heightDimension: .absolute(50.0))
            let header = NSCollectionLayoutBoundarySupplementaryItem(
                layoutSize: footerHeaderSize,
                elementKind: UICollectionView.elementKindSectionHeader,
                alignment: .top)
            
            let columns = 3
            let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0),
                                                  heightDimension: .absolute(2 * padding(.large) + 4))
            let item = NSCollectionLayoutItem(layoutSize: itemSize)
            item.contentInsets = NSDirectionalEdgeInsets(top: 2, leading: 2, bottom: 2, trailing: 2)
            
            let groupHeight = NSCollectionLayoutDimension.absolute(2 * padding(.large) + 4)
            let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0),
                                                   heightDimension: groupHeight)
            let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize, subitem: item, count: columns)
            
            let section = NSCollectionLayoutSection(group: group)
            section.contentInsets = NSDirectionalEdgeInsets(top: 0, leading: 0, bottom: 0, trailing: 0)
            section.boundarySupplementaryItems = [header]
            return section
        }
        return layout
    }
    
    let viewModel: CryptocurrencyViewModel
    
    private let titleLabel = UILabel(text: GlobalConstants.Text.sortAndFilterViewTitle, font: UIFont.systemFont(ofSize: 18), numberOfLines: 1, textAlign: .center, ishidden: false, Color: UIColor.getColor(for: .background) ?? .black)
    
    private let submitButton = UIButton(title: GlobalConstants.Text.sortAndFilterSubmitButtonTitle, color: UIColor.getColor(for: .background) ?? .blue, textColor: UIColor.getColor(for: .secondary) ?? .blue, cornerRadious: padding(.large), font: .systemFont(ofSize: 16))
    
    init(_ viewModel: CryptocurrencyViewModel) {
        self.viewModel = viewModel
        super.init(frame: .zero)
        collectionView.delegate = self
        collectionView.dataSource = self
        backgroundColor = UIColor.getColor(for: .primary)
        submitButton.addTarget(self, action: #selector(handleSubmit), for: .touchUpInside)
        setupView()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        roundCorners(corners: [.topLeft, .topRight], radius: padding(.medium), borderWidth: 1, borderColor: UIColor.getColor(for: .secondary) ?? .black)
    }
    
    private func setupView() {
        
        addSubview(titleLabel)
        titleLabel.anchor(top: safeAreaLayoutGuide.topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, padding: .init(top: padding(.small), left: 0, bottom: 0, right: 0))
        
        addSubview(submitButton)
        submitButton.anchor(top: nil, leading: leadingAnchor, bottom: safeAreaLayoutGuide.bottomAnchor, trailing: trailingAnchor, padding: .init(top: padding(.medium), left: padding(.medium), bottom: padding(.medium), right: padding(.medium)), size: .init(width: 0, height: 2 * padding(.large)))
        
        addSubview(collectionView)
        collectionView.anchor(top: titleLabel.bottomAnchor, leading: leadingAnchor, bottom: submitButton.topAnchor, trailing: trailingAnchor, padding: .init(top: padding(.large), left: padding(.medium), bottom: padding(.medium), right: padding(.medium)))
    }
    
    @objc private func handleSubmit() {
        viewModel.submitFiltersAndSorts()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

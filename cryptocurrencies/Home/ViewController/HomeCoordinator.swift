//
//  HomeCoordinator.swift
//  cryptocurrencies
//
//  Created by amirreza on 7/17/21.
//

import UIKit

class HomeCoordinator: BaseCoordinator {
    
    let router: RouterProtocol
    init(router: RouterProtocol) {
        self.router = router
    }
    
    override func start() {
        
        // prepare the associated view and injecting its viewModel
        let viewModel = CryptocurrencyViewModel()
        let homeVC = HomeViewController(viewModel)
        
        // did select an item from viewModel, define next navigation
        viewModel.didSelectAnItemBindable.bind { [weak self] cryptocurrency in
            guard let self = self, let item = cryptocurrency else { return }
            self.showDetail(of: item, in: self.router)
        }
        
        router.push(homeVC, isAnimated: true, onNavigateBack: self.isCompleted)
    }
    
    /// This will show detail view
    func showDetail(of cryptocurrency: Cryptocurrency, in router: RouterProtocol) {
        let detailCoordinator = DetailCoordinator(router: router, cryptocurrency: cryptocurrency)
        self.start(coordinator: detailCoordinator)
    }
}

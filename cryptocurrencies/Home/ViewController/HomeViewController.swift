//
//  HomeViewController.swift
//  cryptocurrencies
//
//  Created by amirreza on 7/13/21.
//

import UIKit
import Utils

class HomeViewController: UIViewController {
    
    private var viewModel: CryptocurrencyViewModel
    private let cryptocurrencyView: CryptocurrencyView
    private let sortAndFiltersView: SortAndFiltersView
    private var dragLastOffset: CGFloat = 0
    private var dragOffset: CGFloat = 0

    init(_ viewModel: CryptocurrencyViewModel) {
        self.viewModel = viewModel
        sortAndFiltersView = SortAndFiltersView(viewModel)
        cryptocurrencyView = CryptocurrencyView(viewModel)
        super.init(nibName: nil, bundle: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = GlobalConstants.Text.homeTitle
        view.backgroundColor = .getColor(for: .background)
        setupView()
        setupObservers()
    }
    
    private func setupObservers() {
        viewModel.closeFiltersViewBindable.bind { [weak self] _ in
            DispatchQueue.main.async {
                self?.closeFiltersView()
            }
        }
    }
    
    private func setupView() {
        view.addSubview(cryptocurrencyView)
        cryptocurrencyView.fillSuperview()
        
        view.addSubview(sortAndFiltersView)
        sortAndFiltersView.anchor(top: view.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor,
                                  padding: .init(top: -CryptocurrencyView.configurations.sortAndFiltersViewTopPadding , left: 0, bottom: 0, right: 0),
                                  size: .init(width: 0, height: CryptocurrencyView.configurations.sortAndFiltersViewHeight))
        let panGesture = PanDirectionGestureRecognizer(direction: .vertical, target: self, action: #selector(handledpanGesture(_ :)))
        view.addGestureRecognizer(panGesture)
    }
    
    @objc private func handledpanGesture(_ gestureRecognizer: UIPanGestureRecognizer) {
        let translation = gestureRecognizer.translation(in: view)
        switch gestureRecognizer.state {
        case .began:
            dragOffset = dragLastOffset
        case .changed:
            sortAndFiltersView.transform = CGAffineTransform(translationX: 0, y: dragOffset + translation.y)
        case .ended:
            dragLastOffset = dragOffset + translation.y
            handleReleaseAnimation(velocity: gestureRecognizer.velocity(in: view).y)
            dragOffset = 0
        default:
            print("unknown")
        }
    }
    
    private func handleReleaseAnimation(velocity: CGFloat) {
        if velocity < -300 {
            //open
            dragLastOffset = -(CryptocurrencyView.configurations.sortAndFiltersViewHeight - CryptocurrencyView.configurations.sortAndFiltersViewTopPadding )
            UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.5, options: .curveEaseInOut) { [unowned self] in
                self.sortAndFiltersView.transform = CGAffineTransform(translationX: 0, y: self.dragLastOffset)
            }
        }else {
            closeFiltersView()
        }
    }
    
    private func closeFiltersView() {
        dragLastOffset = 0
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.8, options: .curveEaseInOut) { [unowned self] in
            self.sortAndFiltersView.transform = .identity
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

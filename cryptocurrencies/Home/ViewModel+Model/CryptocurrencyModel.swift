//
//  CryptocurrencyModel.swift
//  cryptocurrencies
//
//  Created by amirreza on 7/13/21.
//

import Foundation

struct CryptocurrencyModel {
    
    var criptocurrencies = [Cryptocurrency]()
    
    init() {
        criptocurrencies = DatabaseManager.shared.fetchCryptocurrencies()
    }
    
    struct Response: Codable {
        let data: [ItemModel]
    }
    
    struct ItemModel: Codable {
        let id: Int
        let name: String
        let symbol: String
        let last_updated: String
        let quote: QuoteModel
        let tags: [String]
    }
    
    struct QuoteModel: Codable {
        let USD: USDModel
    }

    struct USDModel: Codable {
        let price: Double
        let market_cap: Double
        let percent_change_24h: Double
    }
    
    struct SortAndFilterDataSourceModel { let title: SectionTitle, values: [String] }
    
    enum SectionTitle: String {
        case sort_dir = "Sort Order"
        case sort = "Sort By"
        case tag = "Filter By Tag"
        case cryptocurrency_type = "Filter By Type"
    }
    
    let sortAndFilterDataSource = [
        SortAndFilterDataSourceModel(title: .sort_dir, values: [CryptocurrencySortOrder.asc.rawValue, CryptocurrencySortOrder.desc.rawValue]),
        SortAndFilterDataSourceModel(title: .sort, values: [CryptocurrencySortKey.name.rawValue, CryptocurrencySortKey.market_cap.rawValue, CryptocurrencySortKey.price.rawValue]),
        SortAndFilterDataSourceModel(title: .tag, values: [CryptocurrencyTag.all.rawValue, CryptocurrencyTag.defi.rawValue, CryptocurrencyTag.filesharing.rawValue]),
        SortAndFilterDataSourceModel(title: .cryptocurrency_type, values: [CryptocurrencyType.all.rawValue, CryptocurrencyType.coins.rawValue, CryptocurrencyType.tokens.rawValue])
    ]
}

//
//  CryptocurrencyViewModel.swift
//  cryptocurrencies
//
//  Created by amirreza on 7/13/21.
//

import Foundation
import CoreData
import Utils

class CryptocurrencyViewModel {
    
    //MARK- Bindables
    
    let closeFiltersViewBindable = Bindable<Bool>()
    let databaseObserverBindable = Bindable<Bool>()
    let didSelectAnItemBindable = Bindable<Cryptocurrency>()
    
    private(set) var isLoading = false
    private(set) lazy var options = CoinMarketCapEndPointModel(limit: CryptocurrencyView.configurations.paginationOffset,
                                                               start: lastIndex, cryptocurrency_type: .all,
                                                               tag: .all,
                                                               sort: .price,
                                                               sort_dir: .desc)
    private(set) var cryptocurrency = CryptocurrencyModel()
    private(set) var lastIndex = 1
    
    init() {
        fetchCryptocurrencies()
    }
    
    //MARK:- Intents
    
    /// Fetches data from remote and updates local database.
    func fetchCryptocurrencies() {
        let endPoint = CoinMarketCapEndpoint.listings(model: options)
        isLoading = true
        NetworkService().request(endPoint: endPoint,
                                 completion: { [weak self] (result: Result<CryptocurrencyModel.Response, NetworkError>) in
            guard let self = self else { return }
            self.isLoading = false
            switch result {
            case .failure(let err):
                print(err.rawValue)
            case .success(let cryptos):
                DatabaseManager.shared.save(cryptos.data, type: self.options.cryptocurrency_type)
            }
        })
    }
    
    func submitFiltersAndSorts() {
        databaseObserverBindable.value = true
        closeFiltersViewBindable.value = true
        fetchCryptocurrencies()
    }
    
    ///Fetches data from remote API and increase `lastIndex` and `options` to the latest update
    func fetchMoreData() {
        lastIndex += CryptocurrencyView.configurations.paginationOffset
        options.start = lastIndex
        fetchCryptocurrencies()
    }
    
    //This function gets called when Sort and Filter Collection view is initializing to decide the selection state of each item.
    func checkIfIsSelected(sectionTitle: CryptocurrencyModel.SectionTitle, itemTitle: String) -> Bool {
        
        switch sectionTitle {
        case .sort_dir:
            return itemTitle == options.sort_dir.rawValue ? true : false
        case .sort:
            return itemTitle == options.sort.rawValue
        case .tag:
            return itemTitle == options.tag.rawValue
        case .cryptocurrency_type:
            return itemTitle == options.cryptocurrency_type.rawValue
        }
    }
    
    ///This function gets called whenever user choose an option from sort and filters view.
    ///It will update the `options` property of the view model to the latest selections.
    func didSelectFilter(with section: CryptocurrencyModel.SectionTitle, itemTitle: String) {
        switch section {
        case .sort_dir:
            options.sort_dir = CryptocurrencySortOrder.allCases.first(where: {$0.rawValue == itemTitle}) ?? .desc
        case .sort:
            options.sort = CryptocurrencySortKey.allCases.first(where: { $0.rawValue == itemTitle }) ?? .price
        case .tag:
            options.tag = CryptocurrencyTag.allCases.first(where: {$0.rawValue == itemTitle}) ?? .all
        case .cryptocurrency_type:
            options.cryptocurrency_type = CryptocurrencyType.allCases.first(where: {$0.rawValue == itemTitle}) ?? .all
        }
    }
}

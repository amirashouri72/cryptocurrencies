//
//  GlobalConstants.swift
//  cryptocurrencies
//
//  Created by amirreza on 7/13/21.
//

import Foundation

struct GlobalConstants {
    
    struct Identifiers {
        static let COIN_MARKET_BASE_URL = "https://pro-api.coinmarketcap.com"
        static let COIN_MARKET_API_KEY = "28a5867d-ff46-4952-b376-0452dc7a7a8d"
        static let DATABASE = "database"
    }
    
    struct Text {
        static let homeTitle = "Cryptocurrencies"
        static let sortAndFilterViewTitle = "Sort & Filters"
        static let sortAndFilterSubmitButtonTitle = "Submit"
    }
}

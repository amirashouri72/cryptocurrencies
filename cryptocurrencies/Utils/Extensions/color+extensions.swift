//
//  color+extensions.swift
//  cryptocurrencies
//
//  Created by amirreza on 7/13/21.
//

import UIKit

extension UIColor {
    enum Theme: String {
        case background
        case primary
        case secondary
    }
    
    static func getColor(for theme: Theme) -> UIColor? {
        UIColor(named: theme.rawValue)
    }
}

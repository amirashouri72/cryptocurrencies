//
//  date+extensions.swift
//  cryptocurrencies
//
//  Created by amirreza on 7/13/21.
//

import Foundation

extension Date {
    //2021-07-13T14:39:02.000Z
    static func getDate(from date: String) -> Date? {
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.dateFormat = "yyyy-mm-dd'T'HH:mm:ss.SSS'Z'"
        return formatter.date(from: date)
    }
}

//
//  DatabaseManager.swift
//  cryptocurrencies
//
//  Created by amirreza on 7/13/21.
//

import Foundation
import CoreData

class DatabaseManager {
    static let shared = DatabaseManager()
    
    let persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: GlobalConstants.Identifiers.DATABASE)
        container.loadPersistentStores { (storeDescription, err) in
            if let err = err {
                fatalError("failed to load database: \(err)")
            }
        }
        return container
    }()
    
    private init() {}
    
    func fetchCryptocurrencies() -> [Cryptocurrency] {
        let context = persistentContainer.viewContext
        let request: NSFetchRequest<Cryptocurrency> = Cryptocurrency.fetchRequest()
        let cryptocurrencies = try? context.fetch(request)
        return cryptocurrencies ?? [Cryptocurrency]()
    }
    
    /// create new cryptocurrency record if there is a record with the given id then it will be updated
    func save(_ cryptocurrencies: [CryptocurrencyModel.ItemModel], type: CryptocurrencyType) {
        let privateContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        let context = persistentContainer.viewContext
        privateContext.parent = context
        let request: NSFetchRequest<Cryptocurrency> = Cryptocurrency.fetchRequest()

        cryptocurrencies.forEach { item in
            request.predicate = NSPredicate(format: "id = %@", NSNumber(value: item.id))
            
            var cryptocurrency: Cryptocurrency
            
            if let fetchedItem = try? privateContext.fetch(request).first {
                //update existing record
                cryptocurrency = fetchedItem
            }else {
                // create new record
                cryptocurrency = Cryptocurrency(context: privateContext)
                cryptocurrency.id = Int64(item.id)
            }
            
            cryptocurrency.name = item.name
            cryptocurrency.last_updated = Date.getDate(from: item.last_updated)
            cryptocurrency.price = item.quote.USD.price
            cryptocurrency.percent_change_24h = item.quote.USD.percent_change_24h
            cryptocurrency.market_cap = item.quote.USD.market_cap
            cryptocurrency.symbol = item.symbol
            cryptocurrency.tags = item.tags.joined(separator: ",")
            cryptocurrency.cryptocurrency_type = type.rawValue
        }
        
        do {
            if privateContext.hasChanges {
                try privateContext.save()
                try context.save()
            }
        } catch let err {
            print("failed update or create Cryptocurrency in local database: ", err)
        }
    }
    
    func clearAllCoreData() {
        let entities = self.persistentContainer.managedObjectModel.entities
        entities.compactMap({ $0.name }).forEach(clearDeepObjectEntity)
    }
    
    func clearDeepObjectEntity(_ entity: String) {
        let context = self.persistentContainer.viewContext

        let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)

        do {
            try context.execute(deleteRequest)
            try context.save()
        } catch {
            print ("There was an error")
        }
    }
}

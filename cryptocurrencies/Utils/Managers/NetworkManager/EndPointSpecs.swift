//
//  EndPointSpecs.swift
//  cryptocurrencies
//
//  Created by amirreza on 7/13/21.
//

import Foundation

protocol EndPointSpecs {
    var baseURL: String { get }
    var path: String { get }
    var httpMethod: String { get }
    var timeOut: Int { get }
    var query: String { get }
}

enum CoinMarketCapEndpoint {
    case listings(model: CoinMarketCapEndPointModel)
}

extension CoinMarketCapEndpoint: EndPointSpecs {
    
    var baseURL: String {
        switch self {
        case .listings:
            return GlobalConstants.Identifiers.COIN_MARKET_BASE_URL
        }
    }
    
    var path: String {
        switch self {
        case .listings:
            return "/listings/latest"
        }
    }
    
    var httpMethod: String {
        switch self {
        case .listings:
            return "GET"
        }
    }
    
    var timeOut: Int {
        switch self {
        case .listings:
            return 30
        }
    }
    
    var query: String {
        switch self {
        case .listings(let model):
            return model.getURLEncodedRepresentation() ?? ""
        }
    }
}

enum CryptocurrencyType: String, CaseIterable {
    case coins
    case tokens
    case all
}

enum CryptocurrencyTag: String, CaseIterable {
    case defi
    case filesharing
    case all
}

enum CryptocurrencySortKey: String, CaseIterable {
    case market_cap
    case name
    case price
}

enum CryptocurrencySortOrder: String, CaseIterable {
    case asc
    case desc
}

struct CoinMarketCapEndPointModel: URLEncodable {
    var limit: Int
    var start: Int
    var cryptocurrency_type: CryptocurrencyType
    var tag: CryptocurrencyTag
    var sort: CryptocurrencySortKey
    var sort_dir: CryptocurrencySortOrder
}

protocol URLEncodable {
    
}

extension URLEncodable {
    
    func getDictionary<T>(returnType: T) -> [String:Any]? {
        let mirror = Mirror(reflecting: self)
        let dict = Dictionary(uniqueKeysWithValues: mirror.children.lazy.map({ (label:String?,value:Any) -> (String,Any)? in
            guard label != nil else { return nil }
            return (label!,value)
        }).compactMap{ $0 })
        return dict
    }
    
    
    func getURLEncodedRepresentation() -> String?{
        
        guard let dictionary = getDictionary(returnType: "") else {
            return nil
        }
        let parameterArray = dictionary.map { (key, value) -> String in
            return "\(key)=\(value)"
        }
        
        return parameterArray.joined(separator:"&")
        
    }
}

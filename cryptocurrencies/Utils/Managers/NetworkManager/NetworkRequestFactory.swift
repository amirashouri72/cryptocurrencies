//
//  NetworkRequestFactory.swift
//  cryptocurrencies
//
//  Created by amirreza on 7/13/21.
//

import Foundation

class NetworkRequestFactory {
    
    func createRequest(with endPoint: EndPointSpecs) -> URLRequest? {
        let urlAdress = urlFromEndPoint(endPoint: endPoint)
        var request = URLRequest(url: URL.init(string: urlAdress)!,
                                        cachePolicy: .reloadIgnoringLocalAndRemoteCacheData,
                                        timeoutInterval: TimeInterval(endPoint.timeOut))
        request.setValue(GlobalConstants.Identifiers.COIN_MARKET_API_KEY, forHTTPHeaderField: "X-CMC_PRO_API_KEY")
        request.httpMethod = endPoint.httpMethod
        return request
    }
    
    private func urlFromEndPoint(endPoint:EndPointSpecs) -> String{
        return endPoint.baseURL + "/v1/cryptocurrency" + endPoint.path + "?" + endPoint.query
    }
}

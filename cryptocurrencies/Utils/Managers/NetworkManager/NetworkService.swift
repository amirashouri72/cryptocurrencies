//
//  NetworkService.swift
//  cryptocurrencies
//
//  Created by amirreza on 7/13/21.
//

import Foundation

class NetworkService {
    
    func request<T: Codable>(endPoint: EndPointSpecs, completion: @escaping (Result<T, NetworkError>) -> ()) {
        
        let session = URLSession.shared
        guard let request = NetworkRequestFactory().createRequest(with: endPoint) else {
            completion(.failure(.badRequest))
            return
        }
        let task = session.dataTask(with: request) { responseData, _, err in
            guard err == nil else {
                completion(.failure(.fetchData))
                return
            }
            
            guard let data = responseData else {
                completion(.failure(.emptyResponseData))
                return
            }
            
            do {
                let object = try JSONDecoder().decode(T.self, from: data)
                completion(.success(object))
            }catch {
                completion(.failure(.failedToSerializedData))
            }
        }
        task.resume()
    }
}

enum NetworkError: String, Error {
    case badRequest
    case fetchData
    case emptyResponseData
    case failedToSerializedData
}

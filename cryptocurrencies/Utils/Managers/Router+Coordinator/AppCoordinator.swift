//
//  AppCoordinator.swift
//  cryptocurrencies
//
//  Created by amirreza on 7/17/21.
//

import UIKit

class AppCoordinator : BaseCoordinator {

    let window : UIWindow

    init(window: UIWindow) {
        self.window = window
        super.init()
    }

    override func start() {
        // preparing root view
        let appearance = UINavigationBarAppearance()
        let color = UIColor.getColor(for: .secondary) ?? .blue
        appearance.titleTextAttributes = [.foregroundColor: color]
        UINavigationBar.appearance().standardAppearance = appearance
        UINavigationBar.appearance().tintColor = color
        
        let router = Router(navigationController: UINavigationController())
        let homeCoordinator = HomeCoordinator(router: router)

        // store child coordinator
        self.start(coordinator: homeCoordinator)

        window.rootViewController = router.navigationController
        window.makeKeyAndVisible()
    }
}

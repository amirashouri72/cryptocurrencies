//
//  Coordinator.swift
//  cryptocurrencies
//
//  Created by amirreza on 7/17/21.
//

import Foundation

protocol Coordinator: AnyObject {
    var childCoordinators: [Coordinator] { get set }
    func start()
}

extension Coordinator {
    
    func store(coordinator: Coordinator) {
        childCoordinators.append(coordinator)
    }
    
    ///This will get called when a view controller pop out of navigation stack
    ///Then coordinator that is responsible for view controller will get dealocated
    func free(coordinator: Coordinator?) {
        guard let coordinator = coordinator else {
            return
        }
        childCoordinators = childCoordinators.filter { $0 !== coordinator }
    }
}

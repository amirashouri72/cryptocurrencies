//
//  Drawable.swift
//  cryptocurrencies
//
//  Created by amirreza on 7/17/21.
//

import UIKit

protocol Drawable {
    var viewController: UIViewController? { get }
}

extension UIViewController: Drawable {
    var viewController: UIViewController? { return self }
}

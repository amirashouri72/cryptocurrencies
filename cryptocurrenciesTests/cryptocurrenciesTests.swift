//
//  cryptocurrenciesTests.swift
//  cryptocurrenciesTests
//
//  Created by amirreza on 7/17/21.
//

import XCTest
@testable import cryptocurrencies

class cryptocurrenciesTests: XCTestCase {
    
    private var viewModel: CryptocurrencyViewModel!
    private var homeViewController: HomeViewController!
    
    override func setUp() {
        super.setUp()
        viewModel = CryptocurrencyViewModel()
        homeViewController = HomeViewController(viewModel)
    }
    
    func testFetchMoreData() {
        let startIndex = viewModel.lastIndex
        
        viewModel.fetchMoreData()
        
        let lastIndex = viewModel.lastIndex
        
        XCTAssertEqual(startIndex + CryptocurrencyView.configurations.paginationOffset , lastIndex)
    }
    
    func testDidSelectFilter() {
        
        viewModel.didSelectFilter(with: .sort, itemTitle: CryptocurrencySortKey.market_cap.rawValue)
        
        let options = viewModel.options

        XCTAssertEqual(options.sort , CryptocurrencySortKey.market_cap)
    }
    
    func testCheckIfIsSelected() {
        
        viewModel.didSelectFilter(with: .sort_dir, itemTitle: CryptocurrencySortOrder.asc.rawValue)
                
        let result = viewModel.checkIfIsSelected(sectionTitle: .sort_dir, itemTitle: CryptocurrencySortOrder.asc.rawValue)
        
        XCTAssertEqual(result, true)
    }
    
    func testSubmitFiltersAndSorts() {
                
        viewModel.closeFiltersViewBindable.value = true
        
        guard let sortAndFilterView = homeViewController.view.subviews.first(where: {$0.isKind(of: SortAndFiltersView.self)}) else { return }
        
        XCTAssertLessThan(sortAndFilterView.frame.minY, CryptocurrencyView.configurations.sortAndFiltersViewHeight)
    }
}
